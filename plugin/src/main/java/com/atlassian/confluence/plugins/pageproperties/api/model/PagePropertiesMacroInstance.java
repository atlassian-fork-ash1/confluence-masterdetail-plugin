package com.atlassian.confluence.plugins.pageproperties.api.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;

/**
 * Simple object to represent the extracted PageProperty details report for one Page Properties macro instance in a
 * content, each table's row in the Page Properties macro will be extracted into a {@link PageProperty} and map by row's
 * heading text (note that row heading text is just plain text and not include html markup. Gets {@link PageProperty}
 * for heading and detail storage format.
 */
public class PagePropertiesMacroInstance {
    private final Map<String, PageProperty> pagePropertyReportRow;

    public PagePropertiesMacroInstance(Map<String, PageProperty> pagePropertyReportRow) {
        this.pagePropertyReportRow = ImmutableMap.copyOf(pagePropertyReportRow);
    }

    /**
     * Gets all the PageProperty rows were extracted from the Page Properties macro's table.
     *
     * @return a {@link Map} mapped by row's heading text and {@link PageProperty}.
     */
    public Map<String, PageProperty> getPagePropertyReportRow() {
        return this.pagePropertyReportRow;
    }

    /**
     * Gets all the PageProperty values were extracted from the Page Properties macro's table.
     *
     * @return {@link List} of {@link PageProperty}.
     */
    public List<PageProperty> getAllPagePropertyReportValues() {
        return Lists.newArrayList(pagePropertyReportRow.values());
    }
}