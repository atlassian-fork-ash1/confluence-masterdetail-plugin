package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Streamable;
import com.atlassian.confluence.content.render.xhtml.Streamables;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.StreamableMacro;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.plugins.createcontent.api.services.CreateButtonService;
import com.atlassian.confluence.renderer.template.TemplateRenderer;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.confluence.extra.masterdetail.DetailsMacro.MASTERDETAIL_RESOURCES;
import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryRenderingStrategy.strategyFor;
import static com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent.Type.MACRO_EXECUTION;
import static com.atlassian.confluence.macro.Macro.BodyType.NONE;
import static com.atlassian.renderer.v2.RenderMode.NO_RENDER;
import static com.atlassian.renderer.v2.RenderUtils.blockError;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * This implements both the old-style {@link com.atlassian.renderer.v2.macro.Macro} and the newer
 * {@link Macro} interface.
 */
@ParametersAreNonnullByDefault
public class DetailsSummaryMacro extends BaseMacro implements StreamableMacro {
    public static final String PARAM_ID = "id";
    private static final String PARAM_SHOW_COMMENTS_COUNT = "showCommentsCount";
    private static final String PARAM_SHOW_LIKES_COUNT = "showLikesCount";
    static final String PARAM_PAGE_SIZE = "pageSize";
    private static final String PARAM_SORT_BY = "sortBy";
    private static final String PARAM_REVERSE_SORT = "reverseSort";
    private static final String PARAM_HEADINGS = "headings";

    private static final String TEMPLATE_PARAM_TOTAL_PAGES = "totalPages";
    private static final String TEMPLATE_PARAM_CURRENT_PAGE = "currentPage";
    private static final String TEMPLATE_PARAM_PAGE_SIZE = "pageSize";
    private static final String TEMPLATE_PARAM_LABEL = "label";
    private static final String TEMPLATE_PARAM_CQL = "cql";
    private static final String TEMPLATE_PARAM_HEADINGS = "headings";
    private static final String TEMPLATE_PARAM_DETAILS = "details";
    private static final String TEMPLATE_PARAM_BLUEPRINT_PRESENT = "blueprintPresent";
    private static final String TEMPLATE_PARAM_LIMIT_REACHED = "limitReached";
    private static final String TEMPLATE_PARAM_WARNING_LINK = "warningLink";

    public static final int DEFAULT_PAGE_SIZE = 30;
    public static final int MAX_PAGESIZE = 1000;
    public static final int PARAM_ID_MAX_LENGTH = 256;

    private final XhtmlContent xhtmlContent;
    private final PageBuilderService pageBuilderService;
    private final I18nResolver i18nResolver;
    private final ContentRetriever contentRetriever;
    private final DetailsSummaryBuilder detailsSummaryBuilder;
    private final SpaceManager spaceManager;
    private final TemplateRenderer templateRenderer;
    private final CreateButtonService createButtonService;
    private final EventPublisher eventPublisher;

    public DetailsSummaryMacro(
            @ComponentImport I18nResolver i18nResolver,
            @ComponentImport XhtmlContent xhtmlContent,
            @ComponentImport SpaceManager spaceManager,
            @ComponentImport PageBuilderService pageBuilderService,
            @ComponentImport CreateButtonService createButtonService,
            ContentRetriever contentRetriever,
            DetailsSummaryBuilder detailsSummaryBuilder,
            @ComponentImport TemplateRenderer templateRenderer,
            @ComponentImport EventPublisher eventPublisher) {
        this.xhtmlContent = xhtmlContent;
        this.pageBuilderService = pageBuilderService;
        this.templateRenderer = templateRenderer;
        this.createButtonService = createButtonService;
        this.contentRetriever = contentRetriever;
        this.i18nResolver = i18nResolver;
        this.detailsSummaryBuilder = detailsSummaryBuilder;
        this.spaceManager = spaceManager;
        this.eventPublisher = eventPublisher;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface.
     * Converts the old wiki markup body into storage format XHTML, then delegates to the newer interface.
     */
    @Override
    public String execute(Map macroParameters, String bodyWikiMarkup, RenderContext renderContext) {
        // TODO SHouldn't we do something with this list?
        final List<RuntimeException> wikiMarkupToStorageConversionErrors = newArrayList();
        final ConversionContext conversionContext = new DefaultConversionContext(renderContext);

        final String storageFormatBody = xhtmlContent.convertWikiToStorage(
                bodyWikiMarkup,
                conversionContext,
                wikiMarkupToStorageConversionErrors
        );

        return execute(macroParameters, storageFormatBody, conversionContext);
    }

    @Override
    public String execute(final Map<String, String> parameters, final String body, final ConversionContext context) {
        return Streamables.writeToString(executeToStream(parameters, Streamables.from(body), context));
    }

    @Override
    public Streamable executeToStream(final Map<String, String> macroParameters, final Streamable body, final ConversionContext conversionContext) {
        try {
            validateContextOwner(conversionContext);
            String id = macroParameters.get(DetailsSummaryMacro.PARAM_ID);
            if (id != null && id.length() > PARAM_ID_MAX_LENGTH)
                throw new MacroExecutionException(i18nResolver.getText("details.error.id.length"));

            final DetailsSummaryMacroMetricsEvent.Builder metrics = DetailsSummaryMacroMetricsEvent
                    .builder(MACRO_EXECUTION)
                    .macroOutputType(conversionContext.getOutputType());

            return streamResponse(metrics, buildTemplateModel(macroParameters, conversionContext, metrics));
        } catch (MacroExecutionException e) {
            // Couldn't determine contents for some reason - render that reason for the user.
            return Streamables.from(blockError(e.getMessage(), ""));
        }
    }

    private Streamable streamResponse(final DetailsSummaryMacroMetricsEvent.Builder metrics, final Map<String, Object> templateModel) {
        pageBuilderService.assembler().resources().requireWebResource(MASTERDETAIL_RESOURCES);
            return writer -> {
            metrics.templateRenderStart();
            templateRenderer.renderTo(writer, MASTERDETAIL_RESOURCES, "Confluence.Templates.Macro.MasterDetail.detailsSummary.soy", templateModel);
            metrics.templateRenderFinish();
            eventPublisher.publish(metrics.build());
        };
    }

    Map<String, Object> buildTemplateModel(final Map<String, String> macroParameters, final ConversionContext conversionContext, final DetailsSummaryMacroMetricsEvent.Builder metrics)
            throws MacroExecutionException {
        final Space currentSpace = spaceManager.getSpace(conversionContext.getSpaceKey());
        if (currentSpace == null) {
            throw new MacroExecutionException("ConversionContext returned invalid space key '" + conversionContext.getSpaceKey() + "'");
        }
        final DetailsSummaryRenderingStrategy renderingStrategy = strategyFor(conversionContext);
        String cql = macroParameters.get(TEMPLATE_PARAM_CQL);

        SearchContext searchContext = conversionContext.getPageContext().toSearchContext().build();

        boolean reverseSort = getBooleanParam(macroParameters, PARAM_REVERSE_SORT);

        List<ContentEntityObject> content = new ArrayList<>();
        //No need to search the contents when using the client-side render as the contents will be loaded by the following rest call.
        //See the success() in metadata-summary.js where the pagination information is setup with the result of rest call as well.
        if (renderingStrategy == DetailsSummaryRenderingStrategy.SERVER_SIDE) {
            ContentRetrieverResult contentResult = contentRetriever.getContentWithMetaData(cql, reverseSort, searchContext, metrics);
            content = contentResult.getRows();
            if (contentResult.isLimited()) {
                macroParameters.put("limitedRows", String.valueOf(ContentRetriever.MAX_RESULTS));
                macroParameters.put("warningLink", "/dosearchsite.action?cql=" + HtmlUtil.urlEncode(cql.replace("currentSpace()", currentSpace.getKey())));
            }
        }
        final DetailsSummaryParameters params = getDetailsSummaryParameters(macroParameters, content, renderingStrategy, conversionContext);
        final PaginatedDetailLines paginatedDetailLines = getPaginatedDetailLines(renderingStrategy, metrics, params, conversionContext.getOutputType());
        final BlueprintParameters blueprint = new BlueprintParameters(macroParameters, currentSpace);

        return buildTemplateModel(macroParameters, paginatedDetailLines, params, blueprint);
    }

    private DetailsSummaryParameters getDetailsSummaryParameters(final Map<String, String> macroParameters, final List<ContentEntityObject> content, final DetailsSummaryRenderingStrategy renderingStrategy, ConversionContext conversionContext)
            throws MacroExecutionException {
        final int currentPage = 0;
        int pageSize;
        // CONF-39021 use max page size in case of export
        if (ConversionContextOutputType.PDF.value().equals(conversionContext.getOutputType())
                || ConversionContextOutputType.WORD.value().equals(conversionContext.getOutputType())
                || ConversionContextOutputType.HTML_EXPORT.value().equals(conversionContext.getOutputType())) {
            pageSize = MAX_PAGESIZE;
        } else {
            pageSize = renderingStrategy.calculatePageSize(macroParameters);
        }

        final String id = macroParameters.get(DetailsSummaryMacro.PARAM_ID);
        if (id != null && id.length() > PARAM_ID_MAX_LENGTH)
            throw new MacroExecutionException(i18nResolver.getText("details.error.id.length"));

        final boolean countComments = getBooleanParam(macroParameters, PARAM_SHOW_COMMENTS_COUNT);
        final boolean countLikes = getBooleanParam(macroParameters, PARAM_SHOW_LIKES_COUNT);
        final String sortBy = macroParameters.get(PARAM_SORT_BY);
        final boolean reverseSort = getBooleanParam(macroParameters, PARAM_REVERSE_SORT);
        final String headings = macroParameters.get(PARAM_HEADINGS);

        return new DetailsSummaryParameters()
                .setPageSize(pageSize)
                .setCurrentPage(currentPage)
                .setCountComments(countComments)
                .setCountLikes(countLikes)
                .setHeadingsString(headings)
                .setSortBy(sortBy)
                .setReverseSort(reverseSort)
                .setContent(content)
                .setId(id);
    }

    private PaginatedDetailLines getPaginatedDetailLines(final DetailsSummaryRenderingStrategy renderingStrategy, final DetailsSummaryMacroMetricsEvent.Builder metrics, final DetailsSummaryParameters params, String outputType) {
        // If we're doing a server-side render (e.g. PDF export, macro browser preview), then we need to fetch the
        // data now so that we can render it as part of our response.
        // For client-side rendering, this data will be fetched later as part of an AJAX request
        if (renderingStrategy == DetailsSummaryRenderingStrategy.SERVER_SIDE) {
            return detailsSummaryBuilder.getPaginatedDetailLines(params, false, metrics, outputType);
        } else {
            return null;
        }
    }

    private Map<String, Object> buildTemplateModel(Map<String, String> macroParameters, @Nullable PaginatedDetailLines paginatedDetailLines,
                                                   final DetailsSummaryParameters params, final BlueprintParameters blueprint) {
        final Map<String, Object> model = newHashMap();

        String firstColumnParam = macroParameters.get("firstcolumn");
        String firstColumnHeading = isBlank(firstColumnParam) ? i18nResolver.getText("detailssummary.heading.title") : firstColumnParam;

        model.put("firstColumnHeading", firstColumnHeading);
        model.put(TEMPLATE_PARAM_CQL, macroParameters.get("cql"));
        model.put(TEMPLATE_PARAM_LABEL, macroParameters.get("label"));
        model.put("macroHeadings", macroParameters.get(PARAM_HEADINGS));
        if (paginatedDetailLines != null) {
            model.put("renderedHeadings", paginatedDetailLines.getRenderedHeadings());
            model.put(TEMPLATE_PARAM_DETAILS, paginatedDetailLines.getDetailLines());
        }
        model.put(PARAM_SORT_BY, params.getSortBy());
        model.put(PARAM_ID, macroParameters.get(PARAM_ID));
        model.put(PARAM_REVERSE_SORT, params.isReverseSort());
        model.put("analyticsKey", macroParameters.get("analytics-key"));
        model.put("showCommentsCount", getBooleanParam(macroParameters, PARAM_SHOW_COMMENTS_COUNT));
        model.put("showLikesCount", getBooleanParam(macroParameters, PARAM_SHOW_LIKES_COUNT));

        model.put(TEMPLATE_PARAM_TOTAL_PAGES, params.getTotalPages());
        model.put(TEMPLATE_PARAM_CURRENT_PAGE, params.getCurrentPage());
        model.put(TEMPLATE_PARAM_PAGE_SIZE, params.getPageSize());
        model.put(TEMPLATE_PARAM_LIMIT_REACHED, macroParameters.get("limitedRows"));
        model.put(TEMPLATE_PARAM_WARNING_LINK, macroParameters.get("warningLink"));

        if (blueprint.isPresent() && (paginatedDetailLines == null ||
                paginatedDetailLines.getDetailLines().isEmpty())) {
            blueprint.decorate(model);
        } else {
            model.put(TEMPLATE_PARAM_BLUEPRINT_PRESENT, false);
        }

        return model;
    }

    private class BlueprintParameters {
        private final Map<String, String> macroParameters;
        private final Space currentSpace;
        private final String blueprintModuleCompleteKey;
        private final String contentBlueprintId;

        private BlueprintParameters(final Map<String, String> macroParameters, final Space currentSpace) {
            this.macroParameters = macroParameters;
            this.currentSpace = currentSpace;
            blueprintModuleCompleteKey = trimToEmpty(macroParameters.get("blueprintModuleCompleteKey"));
            contentBlueprintId = trimToEmpty(macroParameters.get("contentBlueprintId"));
        }

        boolean isPresent() {
            return isNotBlank(blueprintModuleCompleteKey) || isNotBlank(contentBlueprintId);
        }

        void decorate(Map<String, Object> model) {
            model.put(TEMPLATE_PARAM_BLUEPRINT_PRESENT, true);
            model.put("blankTitle", macroParameters.get("blankTitle"));
            model.put("blankDescription", macroParameters.get("blankDescription"));
            model.put("blueprintModuleKey", new ModuleCompleteKey(blueprintModuleCompleteKey).getModuleKey());
            model.put("createFromTemplateHtml", createButtonService.renderBlueprintButton(
                    currentSpace,
                    contentBlueprintId,
                    blueprintModuleCompleteKey,
                    macroParameters.get("createButtonLabel"),
                    null
            ));
        }
    }

    private void validateContextOwner(final ConversionContext conversionContext) throws MacroExecutionException {
        final ContentEntityObject owner = conversionContext.getEntity();
        if (!(owner instanceof SpaceContentEntityObject || owner instanceof Draft))
            throw new MacroExecutionException(i18nResolver.getText("detailssummary.error.must.be.inside.space"));
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public RenderMode getBodyRenderMode() {
        return NO_RENDER;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public boolean hasBody() {
        return false;
    }

    /**
     * From the old {@link com.atlassian.renderer.v2.macro.Macro} interface
     */
    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context) {
        return TokenType.BLOCK;
    }

    /**
     * From the  {@link com.atlassian.confluence.macro.Macro} interface
     */
    @Override
    public BodyType getBodyType() {
        return NONE;
    }

    /**
     * From the  {@link com.atlassian.confluence.macro.Macro} interface
     */
    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    private boolean getBooleanParam(Map<String, String> macroParams, String paramName) {
        return Boolean.parseBoolean(macroParams.get(paramName));
    }

    private Space getCurrentSpace(String spaceKey) {
        return spaceManager.getSpace(spaceKey);
    }
}
