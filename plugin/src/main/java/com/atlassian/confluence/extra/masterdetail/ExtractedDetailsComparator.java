package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.pages.NaturalStringComparator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractedDetailsComparator implements Comparator<ExtractedDetails> {
    final private String key;
    final private boolean reverseSort;
    final private Comparator comparator;
    final private DateFormat dateFormatter;

    private static final String DATE_PATTERN = "(.*)\"(\\d\\d\\d\\d-\\d\\d-\\d\\d)\"(.*)";
    private static Pattern regex = Pattern.compile(DATE_PATTERN, Pattern.DOTALL);

    public ExtractedDetailsComparator(String key, boolean reverseSort) {
        this.key = key;
        this.reverseSort = reverseSort;
        this.comparator = new NaturalStringComparator();
        this.dateFormatter = new SimpleDateFormat("yyyy-MM-dd"); // Not thread-safe
    }

    @Override
    public int compare(ExtractedDetails first, ExtractedDetails second) {
        //special key not in the details map
        boolean titleKey = ("title".equalsIgnoreCase(key));

        String firstValue = getValue(first, titleKey);
        String secondValue = getValue(second, titleKey);

        try {
            Date firstDate = extractDate(firstValue);
            Date secondDate = extractDate(secondValue);

            return !reverseSort ? firstDate.compareTo(secondDate) : secondDate.compareTo(firstDate);
        } catch (ParseException e) {
            // continue with normal sorting
        }

        int comparison = comparator.compare(firstValue, secondValue);
        return reverseSort ? -comparison : comparison;
    }

    private Date extractDate(String value) throws ParseException {
        Matcher regexMatcher = regex.matcher(value);
        if (regexMatcher.find())
            return dateFormatter.parse(regexMatcher.group(2));
        throw new ParseException("Can't extract date", 0);
    }

    private String getValue(ExtractedDetails first, boolean titleKey) {
        if (first == null)
            return "";

        String firstValue = (titleKey ? first.getTitle() : first.getDetailStorageFormat(key));
        if (firstValue == null)
            return "";

        return firstValue;
    }
}
