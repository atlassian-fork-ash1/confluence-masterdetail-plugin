package com.atlassian.confluence.extra.masterdetail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestExtractedDetailsComparator {
    @Test
    public void sortTitle() {
        List<ExtractedDetails> details = setupTitles("W", "a", "", "B", "ab");

        ExtractedDetailsComparator titleComparator = new ExtractedDetailsComparator("title", false);
        details.sort(titleComparator);

        List<String> expected = Arrays.asList("", "a", "ab", "B", "W");

        expectTitles(details, expected);
    }

    @Test
    public void reverseSortTitle() {
        List<ExtractedDetails> details = setupTitles("W", "a", "", "B", "ab");

        ExtractedDetailsComparator reversedComparator = new ExtractedDetailsComparator("title", true);
        details.sort(reversedComparator);

        List<String> expected = Arrays.asList("W", "B", "ab", "a", "");
        expectTitles(details, expected);
    }

    @Test
    public void testSortDate() {
        List<ExtractedDetails> details = setupValues("Date",
                "<div class=\"content-wrapper\"><p><time datetime=\"2015-07-01\" />&nbsp;</p></div>", //0
                "<div class=\"content-wrapper\"><p>Release date: <time datetime=\"2016-07-03\" />&nbsp;</p></div>", //1
                "<div class=\"content-wrapper\"><p><time datetime=\"2017-07-11\" />&nbsp;</p></div>", //2
                "<div class=\"content-wrapper\"><p>Release date: <time datetime=\"2015-08-02\" />&nbsp;</p></div>", //3
                "<div class=\"content-wrapper\"><p><time datetime=\"2015-09-01\" />&nbsp; some thing here</p></div>", //4
                "<div class=\"content-wrapper\"><p><time datetime=\"2015-12-25\" />&nbsp;</p></div>", //5
                "<div class=\"content-wrapper\"><p><time datetime=\"2016-11-29\" />&nbsp;</p></div>"  //6
        );

        ExtractedDetailsComparator comparator = new ExtractedDetailsComparator("Date", false);
        details.sort(comparator);

        /* Correct order:
           2015-07-01
           2015-08-02
           2015-09-01
           2015-12-25
           2016-07-03
           2016-11-29
           2017-07-11
        */
        List<String> expected = Arrays.asList("0", "3", "4", "5", "1", "6", "2");
        expectTitles(details, expected);

        // Revert sorting
        Collections.reverse(expected);

        ExtractedDetailsComparator reversedComparator = new ExtractedDetailsComparator("Date", true);
        details.sort(reversedComparator);
        expectTitles(details, expected);

    }

    @Test
    public void testSortDateComplexWrap() {
        List<ExtractedDetails> details = setupValues("Date",
                "<div class=\"content-wrapper\">\n" +
                        "<p>Planned:&nbsp;<time datetime=\"2016-08-01\" />&nbsp;<br />Actual:&nbsp;<time datetime=\"2016-08-01\" />&nbsp;<ac:image ac:class=\"emoticon emoticon-tick\" ac:alt=\"(tick)\"><ri:url ri:value=\"https://extranet.atlassian.com/s/en_GB/6441/2a5cd8dd0dda6d294a26603e8f4ef1e62362effa/_/images/icons/emoticons/check.png\" /></ac:image></p></div>", //0
                "<div class=\"content-wrapper\">\n" +
                        "<p>Planned:&nbsp;<time datetime=\"2016-08-04\" />&nbsp;<br />Actual:</p></div>", //1
                "<div class=\"content-wrapper\">\n" +
                        "<p>Planned:&nbsp;<time datetime=\"2016-11-20\" />&nbsp;<br />Actual:</p></div></td>\n" +
                        "<td>\n" +
                        "<div class=\"content-wrapper\">", //2
                "Planned:&nbsp;<time datetime=\"2016-08-05\" />&nbsp;<br />Actual:" //3
        );

        ExtractedDetailsComparator comparator = new ExtractedDetailsComparator("Date", false);
        details.sort(comparator);

        List<String> expected = Arrays.asList("0", "1", "3", "2");
        expectTitles(details, expected);

        // Revert sorting
        Collections.reverse(expected);

        ExtractedDetailsComparator reversedComparator = new ExtractedDetailsComparator("Date", true);
        details.sort(reversedComparator);
        expectTitles(details, expected);
    }

    @Test
    public void nonTitleKey() {
        String key = "other";
        List<ExtractedDetails> details = setupValues(key, "zero", "one", "two", "three", "four", "five");

        ExtractedDetailsComparator comparator = new ExtractedDetailsComparator(key, false);
        details.sort(comparator);

        List<String> expected = Arrays.asList("5", "4", "1", "3", "2", "0");
        expectTitles(details, expected);
    }

    private void expectTitles(List<ExtractedDetails> details, List<String> expected) {
        assertThat(details.stream().map(ExtractedDetails::getTitle).collect(toList()), equalTo(expected));
    }

    private List<ExtractedDetails> setupValues(String key, String... values) {
        List<ExtractedDetails> details = newArrayList();
        for (int i = 0; i < values.length; i++) {
            ExtractedDetails detail = mock(ExtractedDetails.class);
            when(detail.getTitle()).thenReturn("" + i);
            when(detail.getDetailStorageFormat(key)).thenReturn(values[i]);
            details.add(detail);
        }
        return details;
    }

    private List<ExtractedDetails> setupTitles(String... titles) {
        List<ExtractedDetails> details = newArrayList();
        for (String title : titles) {
            ExtractedDetails detail = mock(ExtractedDetails.class);
            when(detail.getTitle()).thenReturn(title);
            details.add(detail);
        }
        return details;
    }
}
