package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.AttachmentUpload;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.model.longtasks.LongTaskStatus;
import com.atlassian.confluence.api.model.longtasks.LongTaskSubmission;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.utils.Classpath;
import com.atlassian.confluence.webdriver.pageobjects.component.macro.ExpandMacro;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import java.util.Collections;

import static com.atlassian.confluence.api.model.content.Content.builder;
import static com.atlassian.confluence.api.model.content.ContentType.PAGE;
import static com.atlassian.confluence.api.model.content.Label.builder;
import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.cssSelector;

@RunWith(ConfluenceStatelessTestRunner.class)
public class PagePropertiesReportTest extends AbstractPagePropertiesReportTest {

    @Test
    public void testRenderJiraIssues() {
        // Pages in this space labelled with different label, meaning no cleanup needed.
        final Label label = Label.builder("test-jira").build();
        final Label parentLabel = Label.builder("test-jira-parent").build();
        createNestedPageSummariesWithJiraIssues(label, parentLabel);
        final PagePropertiesReportMacro report = gotoReportPage();

        assertThat(report.getColumnNames(), hasItems("Title", "Priority", "Child Details"));

        final PageElement childReportTable = report.getColumnElements("Child Details").get(0);
        waitUntil(
                "Couldn't load report table in time",
                childReportTable.timed().hasText("Generating Content properties report"),
                is(Boolean.FALSE), by(10000)
        );
        assertThat(childReportTable.find(cssSelector("tbody tr td")).getText(), not("No content found."));
    }

    @Test
    public void detailsUpdateNotCached() {
        Content anotherPage = builder()
                .space(space.get())
                .title("Page with metadata 2")
                .type(PAGE)
                .body(makeDetailsMarkup("Text", "Some other text"), ContentRepresentation.STORAGE)
                .build();

        anotherPage = rest.contentService().create(anotherPage);
        rest.contentLabelService().addLabels(anotherPage.getId(), labelList);

        PagePropertiesReportMacro report = gotoReportPage(2);

        assertThat(report.getTitles(), contains(anotherPage.getTitle(), propertiesPage.getTitle()));
        assertThat(report.getColumnValues("Text"), contains("Some other text", ""));

        updatePropertyPage("<ac:structured-macro ac:name=\"details\"><ac:rich-text-body>\n" +
                "<table><tbody>\n" +
                "<tr>\n" +
                "<th colspan=\"1\">Text</th>\n" +
                "<td colspan=\"1\">Updated text</td>" +
                "</tr>\n" +
                "</tbody></table>" +
                "</ac:rich-text-body></ac:structured-macro>");
        rest.contentService().delete(anotherPage);

        report = gotoReportPage(1);

        assertThat(report.getTitles(), contains(propertiesPage.getTitle()));
        assertThat(report.getColumnValues("Text"), contains("Updated text"));
    }

    @Test
    public void multiplePropertiesAndReportById() {
        updatePropertyPage(makeDetailsMarkup("myId1", false, "Foo1", "Bar1") +
                        makeDetailsMarkup(null, false, "Foo2", "Bar2") +
                        makeDetailsMarkup(null, false, "Foo3", "Bar3"),
                builder("mylabel2").build(),
                builder("mylabel3").build());

        PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getTitles(), hasItem(propertiesPage.getTitle()));
        assertThat(report.getColumnValues("Foo1"), contains("Bar1"));
        assertThat(report.getColumnValues("Foo2"), contains("Bar2"));
        assertThat(report.getColumnValues("Foo3"), contains("Bar3"));

        // no ID should get all properties on a page
        updateReportPage(makeReportMarkup(builder("mylabel2").build(), "", space.get().getKey(), "",
                "Text,Status,Foo1,Foo2,Foo3", null, null));

        report = gotoReportPage(1);
        assertThat(report.getTitles(), hasItem(propertiesPage.getTitle()));
        assertThat(report.getColumnValues("Foo1"), contains("Bar1"));
        assertThat(report.getColumnValues("Foo2"), contains("Bar2"));
        assertThat(report.getColumnValues("Foo3"), contains("Bar3"));
    }

    @Test
    public void onlyPagesWhichUserCanViewGetRendered() {
        Content anotherPage = builder()
                .space(space.get())
                .title("Unrestricted page")
                .type(PAGE)
                .body(makeDetailsMarkup("Text", "Some other text"), ContentRepresentation.STORAGE)
                .build();

        anotherPage = rest.contentService().create(anotherPage);
        rest.contentLabelService().addLabels(anotherPage.getId(), labelList);

        rpc.getSystemComponent().flushIndexQueue();
        product.loginAndView(userWithPageRestrictions.get(), reportPage);
        PagePropertiesReportMacro report = product.getPageBinder().bind(PagePropertiesReportMacro.class);
        report.waitForRowCount(1);

        assertThat(report.getTitles(), allOf(
                hasItem(anotherPage.getTitle()),
                not(hasItem(propertiesPage.getTitle()))
        ));

        // Clean up
        rest.contentService().delete(anotherPage);
    }

    @Test
    public void formattingInProperties() {
        final String property1 = "Foo";
        final String value1 = "Bar";
        final String property2 = "property2";
        final String value2 = "value2";
        updatePropertyPage(makeDetailsMarkup("<strong>" + property1 + "</strong>", value1,
                property2, "<strong>" + value2 + "</strong>"));

        final PagePropertiesReportMacro report = gotoReportPage(1);

        assertThat(report.getColumnNames(), contains("Title", property1, property2));

        assertThat(report.getColumnValues(property1), contains(value1));
        assertThat(report.getColumnValues(property2), contains(value2));

        waitUntilTrue(report.getColumnElements(property2).get(0).find(By.tagName("strong")).timed().isPresent());
    }

    @Test
    public void overlappingPropertiesAreRepeatedForEachContentThatHasThem() {
        final String property1 = "Foo";
        final String value1 = "Bar";
        final String property2 = "property2";
        final String value2 = "value2";
        final String property5 = "property5";
        final String value5 = "value5";

        updatePropertyPage(makeDetailsMarkup(property1, value1, property2, value2, property5, value5));

        final String property3 = "property3";
        final String value3 = "value3";
        final String property4 = "property4";
        final String value4 = "value4";
        final String property6 = "property6";
        final String value6 = "value6";

        Content anotherPage = rest.contentService().create(builder()
                .space(space.get())
                .title("Page with overlapping properties 2")
                .type(PAGE)
                .body(makeDetailsMarkup(property1, value1, property3, value3, property2,
                        value2, property4, value4, property6, value6), ContentRepresentation.STORAGE)
                .build());
        anotherPage = updatePage(null, anotherPage, label);

        final PagePropertiesReportMacro report = gotoReportPage(2);

        assertThat(report.getColumnNames(), contains("Title", property1, property2, property3, property4, property5, property6));
        assertThat(report.getColumnValues("Title"), contains(anotherPage.getTitle(), propertiesPage.getTitle()));
        assertThat(report.getColumnValues(property1), contains(value1, value1));
        assertThat(report.getColumnValues(property2), contains(value2, value2));
        assertThat(report.getColumnValues(property3), contains(value3, EMPTY));
        assertThat(report.getColumnValues(property4), contains(value4, EMPTY));
        assertThat(report.getColumnValues(property5), contains(EMPTY, value5));
        assertThat(report.getColumnValues(property6), contains(value6, EMPTY));

        // Clean up
        rest.contentService().delete(anotherPage);
    }

    @Test
    public void reportRendersMacrosInDetails() {
        String property1 = "property1";
        String value1 = "<ac:macro ac:name=\"cheese\" />";

        updatePropertyPage(makeDetailsMarkup(property1, value1));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getColumnValues(property1), contains("I like cheese!"));
    }

    @Test
    public void shouldGetValueForNestedMacroAndNonAsyncSafe() {
        final String property1 = "property1";
        //contributors macro is not async safe, which would trigger the javascript invocation of endsWith.
        final String value1 = "<ac:macro ac:name=\"contributors\" />";

        updatePropertyPage(makeDetailsMarkup(property1, value1));
        updateReportPage(makeReportMarkup(label));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getColumnValues(property1).get(0),
                allOf(
                        containsString(UserWithDetails.ADMIN.getUsername()),
                        containsString(user.get().getUsername())
                )
        );
    }

    @Test
    public void nonAsyncRenderSafeMacrosAreRenderedOnFirstPageOfResults() {
        final String property1 = "property1";
        final String value1 = "<ac:macro ac:name=\"expand\"><ac:rich-text-body>I'm free</ac:rich-text-body></ac:macro>";

        updatePropertyPage(makeDetailsMarkup(property1, value1));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        final ExpandMacro expandMacro = product.getPageBinder().bind(ExpandMacro.class);
        expandMacro.clickOnExpand();
        waitUntilTrue(Conditions.forSupplier(expandMacro::isExpanded));
        assertThat(report.getColumnValues(property1), contains(containsString("I'm free")));
    }

    @Test
    public void reportRendersImageInDetails() {
        final String fileName = "confluence-is-cool.png";
        final String attachmentName = "bang.png";
        final String column = "image";

        final AttachmentUpload attachment = new AttachmentUpload(Classpath.getFile(fileName), attachmentName, "image/png", "", false, false);
        rest.attachmentService().addAttachments(propertiesPage.getId(), Collections.singleton(attachment));
        updatePropertyPage(makeDetailsMarkup(column, "<ac:image><ri:attachment ri:filename=\"" + attachmentName + "\" /></ac:image>"));

        updateReportPage(makeReportMarkup(label, space.get().getKey(), column));
        final PagePropertiesReportMacro report = gotoReportPage(1);

        final PageElement columnValue = report.getColumnElements(column).get(0);
        final PageElement columnImageValue = columnValue.find(By.tagName("img"));

        waitUntilTrue(columnImageValue.timed().isVisible());
        assertThat(columnImageValue.getAttribute("src"), containsString(attachmentName));
    }

    @Test
    public void reportWithNoContent() {
        final String firstColumnHeading = "Page Titles";

        updateReportPage(makeReportMarkup(Label.builder("label-not-on-any-content").build(),
                null, null, firstColumnHeading, "", null, null));

        final PagePropertiesReportMacro report = gotoReportPage();

        waitUntil(report.getTable().timed().getText(), containsString("No content found"));
        assertThat(report.getColumnNames(), contains(firstColumnHeading));
    }

    @Test
    public void reportWithEmptyPagePropertyKey() {
        updatePropertyPage(makeDetailsMarkup("&nbsp;", "value1"), label);

        String firstColumnHeading = "first col";
        updateReportPage(makeReportMarkup(label, null, null, firstColumnHeading, null, null, null));

        final PagePropertiesReportMacro report = gotoReportPage(1);

        assertThat(report.getColumnNames(), contains(firstColumnHeading, ""));
        assertThat(report.getColumnValues(""), contains("value1"));
    }

    @Test
    public void onlyPageWithMetadataGetsRendered() {
        createPage("pageWithoutMetadata", "<ac:macro ac:name=\"cheese\" />");

        final PagePropertiesReportMacro report = gotoReportPage(1);

        assertThat(report.getColumnNames(), contains("Title", "Foo"));
        assertThat(report.getColumnValues("Foo"), contains("Bar"));
        assertThat(report.getTitles(), contains(propertiesPage.getTitle()));
    }

    @Test
    public void trashedPagesNotInReportAndPagesFromOtherSpaces() {
        final Label otherSpaceLabel = Label.builder("trashedPagesNotInReportAndPagesFromOtherSpaces" + System.currentTimeMillis()).build();

        final Space space2 = rest.spaceService().create(Space.builder()
                .name("space2")
                .key("space2" + System.currentTimeMillis())
                .build(), false);
        Content page2 = rest.contentService().create(Content.builder()
                .space(space2)
                .title("page2" + System.currentTimeMillis())
                .type(ContentType.PAGE)
                .body(makeDetailsMarkup("property1", "value2"), ContentRepresentation.STORAGE)
                .build()
        );

        page2 = updatePage(null, page2, otherSpaceLabel);
        updatePropertyPage(makeDefaultDetailsMarkup(), otherSpaceLabel);
        updateReportPage(makeReportMarkup(otherSpaceLabel, space.get().getKey() + "," + space2.getKey()), otherSpaceLabel);
        PagePropertiesReportMacro report = gotoReportPage(2);

        assertThat(report.getColumnValues("Title"), contains(propertiesPage.getTitle(), page2.getTitle()));
        assertThat(report.getColumnValues("property1"), contains("value1", "value2"));

        final Content trashedPage = createPage("Page that gets Trashed", makeDefaultDetailsMarkup(), otherSpaceLabel);
        // 'spaces' parameter set to '@all' showing trashed pages is the CONFDEV-17283 issue
        final Content reportPageAllSpaces = createPage("Report Content All Spaces",
                makeReportMarkup(otherSpaceLabel, ALL_SPACES), otherSpaceLabel);

        rest.contentLabelService().addLabels(trashedPage.getId(), Collections.singletonList(otherSpaceLabel));

        report = gotoReportPage(3);
        assertThat(getTitles(report), hasItems(propertiesPage.getTitle(), "Page that gets Trashed"));

        // Clean up
        rest.contentService().delete(page2);
        LongTaskSubmission deletedSpace = rest.spaceService().delete(space2);
        waitUntilTrue(Conditions.forSupplier( () -> {
            LongTaskStatus status = rest.longTaskService().get(deletedSpace.getId()).getOrNull();
            return status != null && status.isSuccessful();

        }));
        rpc.getSystemComponent().flushIndexQueue();
        // End cleanup

        report = gotoPage(reportPageAllSpaces, 2);
        assertThat(getTitles(report), containsInAnyOrder(propertiesPage.getTitle(), "Page that gets Trashed"));

        rest.contentService().delete(trashedPage);

        report = gotoReportPage(1);
        assertThat(report.getTitles(), contains(propertiesPage.getTitle()));

        product.viewPage(reportPageAllSpaces);
        report = gotoPage(reportPageAllSpaces, 1);
        assertThat(report.getTitles(), contains(propertiesPage.getTitle()));
    }

    @Test
    public void testMultiplePropertiesWithRepeatedKeys() {
        updateReportPage(makeReportMarkup(label, "id1", space.get().getKey(), "",
                null, null, null));

        // The properties Content has 4 property tables with a repeated key, two with ids.
        updatePropertyPage(makeDetailsMarkup(null, false, "key", "No Id 1") +
                makeDetailsMarkup(null, false, "key", "No Id 2") +
                makeDetailsMarkup("id1", false, "key", "With Id 1") +
                makeDetailsMarkup("id1", false, "key", "With Id 2"));

        PagePropertiesReportMacro report = gotoReportPage(2);
        waitUntil(report.getNumberOfRows(), is(2));
        assertThat(report.getTitles(), contains(propertiesPage.getTitle(), propertiesPage.getTitle()));
        assertThat(report.getColumnValues("key"), contains("With Id 1", "With Id 2"));

        // Viewing with no ID should get all 4 property rows
        updateReportPage(makeReportMarkup(label, "", space.get().getKey(), "",
                null, null, null));

        report = gotoReportPage(4);
        waitUntil(report.getNumberOfRows(), is(4));
        assertThat(report.getTitles(), contains(
                propertiesPage.getTitle(),
                propertiesPage.getTitle(),
                propertiesPage.getTitle(),
                propertiesPage.getTitle()
        ));
        assertThat(report.getColumnValues("key"), contains("No Id 1", "No Id 2", "With Id 1", "With Id 2"));
    }
}
