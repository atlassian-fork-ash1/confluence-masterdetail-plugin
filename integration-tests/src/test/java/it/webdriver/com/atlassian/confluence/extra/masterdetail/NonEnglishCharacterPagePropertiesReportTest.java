package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static org.apache.commons.lang3.StringUtils.join;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
public class NonEnglishCharacterPagePropertiesReportTest extends AbstractPagePropertiesReportTest {

    final static String UMLAUT_A = "\u00E4";
    final static String KOREAN_PROPERTY = "\uC778\uC0AC";
    final static String KOREAN_VALUE = "\uC548\uB155\uD558\uC138\uC694";
    final static String CHINESE_PROPERTY = "\u95EE\u5019";
    final static String CHINESE_VALUE = "\u4F60\u597D";

    @Fixture
    public static PageFixture propertiesPage2Fixture = pageFixture()
            .title("Page with properties 2")
            .space(space)
            .content(makeDetailsMarkup(UMLAUT_A, "2"))
            .author(user)
            .parent(reportPageFixture)
            .build();

    @Fixture
    public static PageFixture propertiesPage3Fixture = pageFixture()
            .title("Page with properties 3")
            .space(space)
            .content(makeDetailsMarkup(UMLAUT_A, "3"))
            .author(user)
            .parent(reportPageFixture)
            .build();

    private static Content propertiesPage2;
    private static Content propertiesPage3;

    @BeforeClass
    public static void addLabelsToExtraPages() {

        propertiesPage2 = propertiesPage2Fixture.get();
        propertiesPage3 = propertiesPage3Fixture.get();

        rest.contentLabelService().addLabels(propertiesPage2.getId(), labelList);
        rest.contentLabelService().addLabels(propertiesPage3.getId(), labelList);
    }

    @Test
    public void nonEnglishSortParameter() {
        //default sort order is last modified, so creating 1,2,3 would normally be listed as 3,2,1
        updatePropertyPage(makeDetailsMarkup(UMLAUT_A, "1"));
        updateReportPage(makeReportMarkup(label, null, null, "Page title",
                UMLAUT_A, null, UMLAUT_A));

        final PagePropertiesReportMacro report = gotoReportPage(3);

        assertThat(report.getColumnValues("Page title"),
                contains(propertiesPage.getTitle(), propertiesPage2.getTitle(), propertiesPage3.getTitle()));
        assertThat(report.getColumnValues(UMLAUT_A), contains("1", "2", "3"));
    }

    @Test
    public void nonEnglishProperties() {
        updatePropertyPage(makeDetailsMarkup(KOREAN_PROPERTY, KOREAN_VALUE, CHINESE_PROPERTY, CHINESE_VALUE));
        final PagePropertiesReportMacro report = gotoReportPage(3);

        assertThat(report.getColumnValues("Title"), contains(propertiesPage.getTitle(),
                propertiesPage3.getTitle(), propertiesPage2.getTitle()));
        assertThat(report.getColumnValues(CHINESE_PROPERTY), hasItem(CHINESE_VALUE));
        assertThat(report.getColumnValues(KOREAN_PROPERTY), hasItem(KOREAN_VALUE));
    }

    @Test
    public void nonEnglishIdParameter() {
        updatePropertyPage(makeDetailsMarkup(UMLAUT_A, Boolean.FALSE, UMLAUT_A, "1"));
        propertiesPage2 = updatePage(makeDetailsMarkup(UMLAUT_A, Boolean.FALSE, UMLAUT_A, "2"), propertiesPage2);
        propertiesPage3 = updatePage(makeDetailsMarkup(UMLAUT_A, Boolean.FALSE, UMLAUT_A, "3"), propertiesPage3);

        updateReportPage(makeReportMarkup(label, UMLAUT_A, space.get().getKey(),
                "Page title", null, null, "title"));

        final PagePropertiesReportMacro report = gotoReportPage(3);

        assertThat(report.getColumnValues("Page title"), contains(
                propertiesPage.getTitle(), propertiesPage2.getTitle(), propertiesPage3.getTitle()));
        assertThat(report.getColumnValues(UMLAUT_A), contains("1", "2", "3"));
    }

    @Test
    public void nonEnglishHeadingParameter() {
        updatePropertyPage(makeDetailsMarkup(KOREAN_PROPERTY, KOREAN_VALUE, CHINESE_PROPERTY, CHINESE_VALUE));
        updateReportPage(makeReportMarkup(label, null, null,
                "Page title", join(KOREAN_PROPERTY), null, null));
        final PagePropertiesReportMacro report = gotoReportPage(3);

        assertThat(report.getColumnValues("Page title"),
                contains(propertiesPage.getTitle(), propertiesPage3.getTitle(), propertiesPage2.getTitle()));
        assertThat(report.getColumnValues(KOREAN_PROPERTY), hasItem(KOREAN_VALUE));
        assertThat(report.hasColumn(CHINESE_PROPERTY), is(false));
    }

    @Test
    public void reportReportsPropertyIgnoringNbspAndSpans() {
        final String property1 = "property1";
        final String value1 = "value1";
        final String value2 = "value2";
        final String value3 = "value3";
        final String nbsp = "&nbsp;";

        updatePropertyPage(makeDetailsMarkup(property1 + nbsp, value1));
        propertiesPage2 = updatePage(makeDetailsMarkup("<span>" + property1 + "</span>", value2), propertiesPage2);
        propertiesPage3 = updatePage(makeDetailsMarkup(property1, value3), propertiesPage3);

        updateReportPage(makeReportMarkup(label, null, space.get().getKey(), null,
                property1, null, property1));
        final PagePropertiesReportMacro report = gotoReportPage(3);

        assertThat(report.getColumnValues("Title"),
                contains(propertiesPage.getTitle(), propertiesPage2.getTitle(), propertiesPage3.getTitle()));
        assertThat(report.getColumnValues("property1"), contains(value1, value2, value3));
    }
}
